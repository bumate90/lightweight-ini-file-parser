#include <fstream>
#include <iostream>
#include "IniParser.h"

IniParser::IniParser(const char* fileNameStr) : fileName(fileNameStr), numOfSections(0){
	bool retVal = parseFile();

	//file reading successful?
	if (retVal)
	{
		printResults();
	}
}

//Opens input file and starts parsing it
bool IniParser::parseFile() {
	std::string line;
	std::ifstream myfile(fileName);
	bool retVal = false;
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			parseLine(line);
		}
		myfile.close();
		retVal = true;
	}
	else
	{
		std::cout << "Unable to open file";
		retVal = false;
	}
	return retVal;
}

void IniParser::reParse() {
	//cleanup process - BEGIN
	for (unsigned short i = 0; i < numOfSections; i++)
	{
		parsedSections[i].parsedElements.erase(parsedSections[i].parsedElements.begin(), parsedSections[i].parsedElements.end());
	}
	parsedSections.erase(parsedSections.begin(), parsedSections.end());
	numOfSections = 0;
	//cleanup process - END
	
	//reopen input file and parse it again
	bool retVal = parseFile();
	//file reading successful?
	if (retVal)
	{
		printResults();
	}
}

void IniParser::parseLine(std::string &line) {
	//Ignore empty lines
	if (line.length() > 1)
	{
		//Ignore commented lines
		if(line[0] != '#')
		{
			size_t pos = line.find_first_of('=', 0);
			if(std::string::npos != pos)
			{
				//Element found;
				parsedSections[numOfSections - 1].AddElement(IniElement(line.substr(0, pos),
																		line.substr(pos + 1, line.length())));
			}
			else if((std::string::npos != line.find_first_of('[', 0)) && 
				    (std::string::npos != line.find_first_of(']', 0)))
			{
				//Section found;
				parsedSections.push_back(IniSection(line));
				numOfSections++;
			}
			else
			{
				//Unrecognized token
				std::cout <<"ERROR: unrecognized token!"<< std::endl;
			}
		}
	}
}

void IniParser::printResults() {
	for (auto& x : parsedSections)
	{
		std::cout << x.sectionName << std::endl;
		for (auto& y : x.parsedElements)
		{
			std::cout << y.keyName <<" = "<< y.keyValue << std::endl;
		}
	}
}