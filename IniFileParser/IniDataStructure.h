#pragma once
#include <string>
#include <vector>

struct IniElement {
	std::string keyName;
	std::string keyValue;
	IniElement();
	IniElement(const std::string& attr, const std::string& val);
};

struct IniSection {
	std::string sectionName;
	std::vector<IniElement> parsedElements;
	explicit IniSection(const std::string& name);
	void AddElement(const IniElement& ie);
};