#include "IniDataStructure.h"

//IniElement	
IniElement::IniElement() :keyName(""), keyValue("") {};
IniElement::IniElement(const std::string& attr, const std::string& val) :keyName(attr), keyValue(val) {};

//IniSection
IniSection::IniSection(const std::string& name) :sectionName(name) {};
void IniSection::AddElement(const IniElement& ie)
{
	parsedElements.push_back(ie);
}