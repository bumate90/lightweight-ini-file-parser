# Lightweight INI file parser

This is a lightweight INI file parser, with minimal error handling.

Parses an INI file(given as a command line argument) and prints it to the 
console. Also monitors the directory
for changes and reparses the file if necessary.

Usage: IniFileParser.exe [filename]

 [filename] specifies the file to be parsed and monitored.
 
 This file should be present in the same directory next to the executable...
 
 Tested on Windows 10 Pro.
 Visual studio 2017 community edition solution is provided as part of the 
 repository.