#include <iostream>
#include <fstream>
#include <Windows.h>
#include <tchar.h>
#include "IniParser.h"

//helper function - checks whether the given file exists or not
inline bool exists_test0(const char* name) {
	std::ifstream f(name);
	return f.good();
}

//helper functions converts std::string to std::wstring
std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

//Infinite loop 
//par1: parser to be called upon event - write directory
//par2: directory to be monitored for change
void monitor(IniParser& parser, LPCWSTR &str) {
	HANDLE  ChangeHandle = FindFirstChangeNotification(str, FALSE, FILE_NOTIFY_CHANGE_LAST_WRITE);
	for (;;)
	{
		DWORD Wait = WaitForSingleObject(ChangeHandle, INFINITE);
		if (Wait == WAIT_OBJECT_0)
		{
			parser.reParse();
			FindNextChangeNotification(ChangeHandle);
		}
		else
		{
			break;
		}
	}
}

int main(int argc, char* argv[])
{
	//Working directory as a string
	std::string base = (".\\");
	std::wstring stemp = s2ws(base);
	LPCWSTR workingDirWstr = stemp.c_str();

	//Making sure ini file is provided as command line argument
	if (argc == 2)
	{
		if (exists_test0(argv[1]))
		{
			//Main operation
			IniParser parser(argv[1]);
			monitor(parser, workingDirWstr);
		}
		else
		{
			std::cout << "File does not exist!" << std::endl;
		}
	}
	else
	{
		std::cout << "Usage:\n\r IniFileParser.exe [filename]\n\r [filename] specifies the file to be parsed and monitored.\n\r";
		std::cout << " This file should be present in the same directory next to the executable..." << std::endl;
	}
	return 0;
}
