#pragma once
#include "IniDataStructure.h"

class IniParser {

private:
	const char* fileName;
	unsigned short numOfSections;
	std::vector<IniSection> parsedSections;
	//Internal helper functions
	bool parseFile();	
	void parseLine(std::string &line);
public:
	//Constructor - input: name of the ini file
	explicit IniParser(const char* fileNameStr);
	//Copying disabled...
	IniParser & operator=(const IniParser&) = delete;
	IniParser(const IniParser&) = delete;
	//Called upon event - file change
	void reParse();	
	//Results
	void printResults();
};

